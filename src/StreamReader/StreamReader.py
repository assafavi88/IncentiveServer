#!/usr/bin/env python

import sys
sys.path.append('..')

import time

import pusherclient
import MySQLdb
import datetime
from dateutil.parser import parse
from Config import Config
import logging
from logging.handlers import RotatingFileHandler
log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
log_formatter.converter = time.gmtime
cnf = Config.Config().conf
# Add a logging handler so we can see the raw communication data
import logging
root = logging.getLogger()
root.setLevel(logging.INFO)
ch = logging.StreamHandler(sys.stdout)
root.addHandler(ch)

global pusher

logFile = cnf['strmLog']
my_handler = RotatingFileHandler(logFile, mode='a', maxBytes=1*1024*1024, backupCount=50, encoding=None, delay=0)
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.INFO)

app_log = logging.getLogger('root')
app_log.setLevel(logging.INFO)
app_log.addHandler(my_handler)

def sql(user_id, city_name, country_name, project, subjects, created_at):
    local_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    # connect
    conn = MySQLdb.connect(host=cnf['host'], user=cnf['user'], passwd=cnf['password'], db=cnf['db'])

    cursor = conn.cursor()
    try:
        datet=parse(created_at)
        time=datetime.datetime(datet.year,datet.month,datet.day,datet.hour,datet.minute,datet.second)
        cursor.execute("""INSERT INTO stream (user_id,project,subjects,created_at,country_name,city_name,local_time) VALUES (%s,%s,%s,%s,%s,%s,%s)""",
                       (user_id,project,subjects,time,country_name,city_name,local_time))
        conn.commit()
    except MySQLdb.Error as e:
        app_log.info(e)
        conn.rollback()
    conn.close()

def channel_callback(data):
    
    dataStr=str(data)
    otherName=dataStr.split(',')

    x={}
    x['project']=dataStr.split('project')[1].split(',')[0][2:].replace('"','')
    print ("\n"+x['project'] +"\n")
    if (x['project']=="galaxy_zoo"):
      x['user_id']=dataStr.split('user_id')[1].split(',')[0][2:].replace('"','')
      x['city_name']=dataStr.split('city_name')[1].split(',')[0][2:].replace('"','')
      x['country_name']=dataStr.split('country_name')[1].split(',')[0][2:].replace('"','')
      x['subjects']=dataStr.split('subjects')[1].split(',')[0][2:].replace('"','')
      x['created_at']=dataStr.split('created_at')[1].split(',')[0][2:].replace('"','')
      sql(x['user_id'],x['city_name'],x['country_name'],x['project'],x['subjects'],x['created_at'])
      print x

def connect_handler(data):
    channel = pusher.subscribe("ouroboros")  
    channel.bind('classification', channel_callback)
    


        
if __name__ == "__main__":
   while True:
        try:
            app_log.info("starting stream\n")
	    appkey = "79e8e05ea522377ba6db"
	    pusher = pusherclient.Pusher(appkey)
	    pusher.connection.bind('pusher:connection_established', connect_handler)
	    pusher.connect()
	
	    while True:
	        time.sleep(1)
	        if (pusher.connection.state!="connected"):
		  break;
		 
		
	except:
            app_log.info("Stream Crashed\n")
            app_log.info(sys.exc_info())
            print sys.exc_info()
            continue	
